package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import static org.junit.Assert.*;

public class CreateSheetCommandTest {

    @Test
    public void createSheet() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        ICommand command = new CreateSheetCommand(book, "testsheet");
        command.execute();

        assertEquals(true, book.sheetExists("testsheet"));
    }

    @Test
    public void undoCreateSheet() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        ICommand command = new CreateSheetCommand(book, "testsheet");
        command.execute();
        assertEquals(true, book.sheetExists("testsheet"));

        command.unexecute();
        assertEquals(false, book.sheetExists("testsheet"));

    }

}
