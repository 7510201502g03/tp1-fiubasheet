package ar.fiuba.tdd.fiubasheet;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class CommandHandlerTest {

    private class StateClass {
        int value;

        public StateClass(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void add() {
            value++;
        }

        public void substract() {
            value--;
        }
    }

    private class Command implements ICommand {
        StateClass state;

        public Command(StateClass state) {
            this.state = state;
        }

        @Override
        public void execute() {
            state.add();
        }

        @Override
        public void unexecute() {
            state.substract();
        }
    }

    private CommandHandler commandHandler = new CommandHandler();
    private int value = 0;
    private StateClass stateClass = new StateClass(value);
    private Command command1;


    @Before
    public void setUp() throws Exception {
        command1 = new Command(stateClass);
        commandHandler.doCommand(command1);
    }

    @Test
    public void testDoCommand() throws Exception {
        value = stateClass.getValue();
        assertEquals(1, value);
    }

    @Test
    public void testUndo() throws Exception {
        commandHandler.undo();
        value = stateClass.getValue();
        assertEquals(0, value);

    }
    
    @Test
    public void testRedo() throws Exception {
        commandHandler.undo();
        commandHandler.redo();
        value = stateClass.getValue();
        assertEquals(1, value);
    }

    @Test
    public void testDoMultipleCommands()  throws Exception  {
        Command command2 = new Command(stateClass);
        commandHandler.doCommand(command2);
        Command command3 = new Command(stateClass);
        commandHandler.doCommand(command3);
        value = stateClass.getValue();
        assertEquals(3, value);
    }

    @Test
    public void testMultipleUndo()  throws Exception  {
        Command command2 = new Command(stateClass);
        commandHandler.doCommand(command2);
        Command command3 = new Command(stateClass);
        commandHandler.doCommand(command3);
        commandHandler.undo();
        commandHandler.undo();
        value = stateClass.getValue();
        assertEquals(1, value);
    }

    @Test
    public void tesUndoRedo() throws Exception  {
        Command command2 = new Command(stateClass);
        commandHandler.doCommand(command2);
        Command command3 = new Command(stateClass);
        commandHandler.doCommand(command3);
        commandHandler.undo();
        commandHandler.undo();
        commandHandler.redo();
        commandHandler.redo();
        value = stateClass.getValue();
        assertEquals(3, value);
    }

    @Test
    public void testMultipleCommandCalls() throws Exception  {
        commandHandler.doCommand(command1);
        commandHandler.doCommand(command1);
        commandHandler.doCommand(command1);
        commandHandler.undo();
        commandHandler.redo();
        value = stateClass.getValue();
        assertEquals(4, value);
    }

    @Test
    public void testCommandClearsRedo() throws Exception {
        commandHandler.doCommand(command1);

        commandHandler.undo();
        commandHandler.doCommand(command1);
        commandHandler.redo();
        value = stateClass.getValue();
        assertEquals(2, value);
    }

}