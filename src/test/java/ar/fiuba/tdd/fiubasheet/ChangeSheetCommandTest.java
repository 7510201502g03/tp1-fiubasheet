package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import static org.junit.Assert.*;

public class ChangeSheetCommandTest {

    @Test
    public void changeActiveSheet() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        book.addSheet("sheetOne");
        book.addSheet("sheetTwo");

        book.setActiveSheet("sheetOne");
        assertEquals("sheetOne", book.getActiveSheet());

        ICommand command = new ChangeSheetCommand(book, "sheetTwo");
        command.execute();
        assertEquals("sheetTwo", book.getActiveSheet());
    }

    @Test
    public void undoChangeActiveSheet() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        book.addSheet("sheetOne");
        book.addSheet("sheetTwo");

        book.setActiveSheet("sheetOne");
        assertEquals("sheetOne", book.getActiveSheet());

        ICommand command = new ChangeSheetCommand(book, "sheetTwo");
        command.execute();
        assertEquals("sheetTwo", book.getActiveSheet());

        command.unexecute();
        assertEquals("sheetOne", book.getActiveSheet());

    }
}
