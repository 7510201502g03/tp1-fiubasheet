package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CellFormatTest {

    private static final double DELTA = 0.00001;

    @Test
    public void numberFormat() {
        ICellFormat format = new CellFormatNumber();

        assertEquals(false, format.matchesExpression("foo"));
        assertEquals(false, format.matchesExpression("!!2323.-.p"));

        assertEquals(true, format.matchesExpression("2"));
        assertEquals(true, format.matchesExpression("2.0"));

        assertEquals(2, Double.parseDouble(format.valueFromFormattedExpression("2.0")), DELTA);
        assertEquals(2, Double.parseDouble(format.valueFromFormattedExpression("2")), DELTA);

        format.customize("decimal", "2");
        assertEquals("2.00", format.formatValue("2.0"));

        format.customize("decimal", "0");
        assertEquals("2", format.formatValue("2.0"));

    }

    @Test
    public void moneyFormat() {
        ICellFormat format = new CellFormatMoney();

        assertEquals(false, format.matchesExpression("foo"));
        assertEquals(false, format.matchesExpression("200"));

        assertEquals(true, format.matchesExpression("$2"));
        assertEquals(true, format.matchesExpression("$2.0"));

        assertEquals(2, Double.parseDouble(format.valueFromFormattedExpression("$2.0")), DELTA);
        assertEquals(2, Double.parseDouble(format.valueFromFormattedExpression("$2")), DELTA);

        format.customize("symbol", "USD ");
        format.customize("decimal", "2");
        assertEquals("USD 2.00", format.formatValue("2.0"));

        format.customize("symbol", "USD ");
        format.customize("decimal", "0");
        assertEquals("USD 2", format.formatValue("2.0"));

    }


 /*   @Test
    public void dateFormat() {
        ICellFormat format = new CellFormatDate();

        assertEquals(false, format.matchesExpression("foo"));
        assertEquals(false, format.matchesExpression("200"));

        assertEquals(true, format.matchesExpression("02-05-1990"));
        assertEquals(true, format.matchesExpression("2-5-90"));

        String val = format.valueFromFormattedExpression("2-5-90");
        assertEquals("02-05-90", format.formatValue(val, ""));

        format.customize("format", "MM-dd-yy");
        assertEquals("05-02-90", format.formatValue(val, ""));

    } */
}
