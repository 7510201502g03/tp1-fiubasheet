package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class CalculatorTest {

    private static final double DELTA = 0.00001;

    // Mock para simular acceso a celdas
    private class MockCellReader implements ICellValueReader {

        // Hash para guardar valores de celdas mock
        private HashMap<String, Double> cellValues = new HashMap<>();

        // Cargamos las celdas que vamos a usar en los tests
        public MockCellReader() {
            cellValues.put("A1", 4.0);
            cellValues.put("A2", 2.0);
            cellValues.put("A3", 3.0);
            cellValues.put("B1", 5.0);
            cellValues.put("B2", -2.0);
            cellValues.put("B3", -3.0);
        }

        // Implementamos la interfaz ICellReader
        public Double getValue(String cellId, String sheet) {
            return cellValues.get(cellId);
        }

        public String getValueLiteral(String cellId, String sheet) {
            return Double.toString(cellValues.get(cellId));
        }
    }

    // Single instance para todos los tests
    private Calculator calculator = new Calculator();

    // Armamos el reader para los tests
    public CalculatorTest() {
        calculator.setBookReader(new MockCellReader());
    }

    // Calculator tests
    @Test
    public void constantsBinSum() {
        Double res = Double.parseDouble(calculator.eval("2 + 3", ""));
        assertEquals(5, res, DELTA);
    }

    @Test
    public void constantsBinSub() {
        Double res = Double.parseDouble(calculator.eval("2 - 3", ""));
        assertEquals(-1, res, DELTA);
    }

    @Test
    public void constantsMultiSum() {
        Double res = Double.parseDouble(calculator.eval("2 + 3 + 4", ""));
        assertEquals(9, res, DELTA);
    }

    @Test
    public void constantsMultiSub() {
        Double res = Double.parseDouble(calculator.eval("2 - 3 - 4", ""));
        assertEquals(-5, res, DELTA);
    }

    @Test
    public void mixedBinSum() {
        Double res = Double.parseDouble(calculator.eval("2 + A2", ""));
        assertEquals(4, res, DELTA);
    }

    @Test
    public void mixedBinSub() {
        Double res = Double.parseDouble(calculator.eval("2 - A2", ""));
        assertEquals(0, res, DELTA);
    }

    @Test
    public void cellBinSum() {
        Double res = Double.parseDouble(calculator.eval("A2 + A3", ""));
        assertEquals(5, res, DELTA);
    }

    @Test
    public void cellBinSub() {
        Double res = Double.parseDouble(calculator.eval("A3 - A2", ""));
        assertEquals(1, res, DELTA);
    }

    @Test
    public void mixedCellConstantOps() {
        Double res = Double.parseDouble(calculator.eval("5 + A3 - A2 + B2", ""));
        assertEquals(4, res, DELTA);
    }

    @Test
    public void calculationGetDependencies() {
        ArrayList<String> dependencies = calculator.getDependencies("A4 + 5 + B1", "default");
        assertEquals(2, dependencies.size());
        assertEquals(true, dependencies.contains(Book.makeFullCellId("A4", "default")));
        assertEquals(true, dependencies.contains(Book.makeFullCellId("B1", "default")));

    }


    @Test
    public void averageRange() {
        Double res = Double.parseDouble(calculator.eval("AVERAGE(A1:B2)", ""));
        assertEquals(2.25, res, DELTA);
    }

    @Test
    public void averageGetDependencies() {
        ArrayList<String> dependencies = calculator.getDependencies("AVERAGE(A1:B2)", "default");
        assertEquals(4, dependencies.size());
        assertEquals(true, dependencies.contains(Book.makeFullCellId("A1", "default")));
        assertEquals(true, dependencies.contains(Book.makeFullCellId("B1", "default")));
        assertEquals(true, dependencies.contains(Book.makeFullCellId("A2", "default")));
        assertEquals(true, dependencies.contains(Book.makeFullCellId("B2", "default")));

    }

    @Test
    public void maxRange() {
        Double res = Double.parseDouble(calculator.eval("MAX(A1:B2)", ""));
        assertEquals(5.0, res, DELTA);
    }

    @Test
    public void minRange() {
        Double res = Double.parseDouble(calculator.eval("MIN(A1:B2)", ""));
        assertEquals(-2.0, res, DELTA);
    }

    @Test
    public void concatTwoCells() {
        String res = calculator.eval("CONCAT(A1,A2)", "");
        assertEquals(res, "4.02.0");
    }

    @Test
    public void concatTwoCellsDependencies() {
        ArrayList<String> res = calculator.getDependencies("CONCAT(A1,A2)", "default");
        assertEquals(2, res.size());
        assertEquals(true, res.contains(Book.makeFullCellId("A1", "default")));
        assertEquals(true, res.contains(Book.makeFullCellId("A2", "default")));
    }
}
