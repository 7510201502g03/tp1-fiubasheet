package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class CellEventNotifierTest {

    private class ConcreteSubscriber implements Subscriber {
        private String name;

        public ConcreteSubscriber(String nomb) {
            name = nomb;
        }

        @Override
        public void update() {
            name += " updated";
        }

        public String getName() {
            return name;
        }
    }

    Subscriber a1 = new ConcreteSubscriber("A1");
    Subscriber a2 = new ConcreteSubscriber("A2");
    Subscriber b3 = new ConcreteSubscriber("B3");
    Subscriber b4 = new ConcreteSubscriber("B4");
    CellEventNotifier cen = new CellEventNotifier();


    @Test
    public void testNoSubscribers() throws Exception {
        ArrayList<Subscriber> subs = cen.getSubscribers("A1");
        assertEquals(null, subs);
    }

    @Test
    public void testSubscribe() throws Exception {
        cen.subscribe("A1", a2);
        cen.subscribe("A1", b3);
        ArrayList<Subscriber> subs = cen.getSubscribers("A1");
        assertEquals(2, subs.size());
    }

    @Test
    public void testUnsubscribe() throws Exception {
        cen.subscribe("A1", a2);
        cen.subscribe("A1", b3);
        cen.unsubscribe(a2);
        ArrayList<Subscriber> subs = cen.getSubscribers("A1");
        assertEquals(1, subs.size());
    }

    @Test
    public void testNotify() throws Exception {
        cen.subscribe("A1", a2);
        cen.subscribe("B3", a2);
        cen.subscribe("B3", b4);
        cen.notifySubscribers("A1");
        ConcreteSubscriber a22 = (ConcreteSubscriber) a2;
        assertEquals("A2 updated", a22.getName());
    }


}