package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import static org.junit.Assert.*;

public class ImporterExporterFactoryTest {

    @Test
    public void getImpExp() {
        String fileName = "arch.json";
        ImporterExporter ieConcreto = ImporterExporterFactory
                .getImpExp(fileName);

        assertEquals(true, ieConcreto instanceof Json);

        fileName = "arch.csv";
        ieConcreto = ImporterExporterFactory.getImpExp(fileName);

        assertEquals(true, ieConcreto instanceof Csv);
    }

}
