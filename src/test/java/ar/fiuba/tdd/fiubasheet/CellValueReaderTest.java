package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CellValueReaderTest {

    private static final double DELTA = 0.00001;

    @Test
    public void getValueFullID() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        book.addSheet("Sheet");
        book.setActiveSheet("Sheet");
        book.setOnActiveSheet("A1", "=1 + 1");
        CellValueReader cell = new CellValueReader(book);

        assertEquals(2.0, cell.getValue("!Sheet.A1", "Sheet"), DELTA);

    }

    @Test
    public void getValueNotFullID() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        book.addSheet("Sheet");
        book.setActiveSheet("Sheet");
        book.setOnActiveSheet("A1", "=1 + 1");
        CellValueReader cell = new CellValueReader(book);

        assertEquals(2.0, cell.getValue("A1", "Sheet"), DELTA);

    }

}
