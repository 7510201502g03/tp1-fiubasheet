package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class BookTest {

    private static final double DELTA = 0.00001;

    // Single instance para todos los tests
    private Book book;

    private void newBook() {
        book = new Book(new Calculator(), new CellEventNotifier());
    }

    // Book tests
    @Test
    public void setAndGetActiveSheet() {
        newBook();

        book.setOnActiveSheet("A1", "foo");
        assertEquals("foo", book.getFromActiveSheet("A1").getAsString());

    }

    @Test
    public void getBlankFromUnset() {
        newBook();

        assertEquals("", book.getFromActiveSheet("A1").getAsString());

    }

    @Test
    public void setAndGetNewSheet() {
        newBook();

        book.addSheet("testsheet");
        book.setActiveSheet("testsheet");
        book.setOnActiveSheet("A1", "foo");
        assertEquals("foo", book.getFromActiveSheet("A1").getAsString());
        assertEquals("foo", book.get("A1", "testsheet").getAsString());
        assertEquals("testsheet", book.getActiveSheet());

    }

    @Test(expected = IllegalArgumentException.class)
    public void createInvalidSheet() {

        newBook();

        book.addSheet("+:!__232");

    }

    @Test(expected = IllegalArgumentException.class)
    public void getInvalidCell() {
        newBook();

        book.getFromActiveSheet("2323--!QQ@@");

    }

    @Test(expected = IllegalArgumentException.class)
    public void setInvalidSheet() {
        newBook();

        book.set("A1", "!invalidSheet", "foo");

    }

    @Test
    public void basicFormula() {
        newBook();

        book.setOnActiveSheet("A1", "= 1 + 1");
        assertEquals(2.0,
                Float.parseFloat(book.getFromActiveSheet("A1").getAsString()),
                DELTA);

    }

    @Test
    public void isFullCellId() {
        newBook();
        String cellID = "!Stuff.A1";
        assertEquals(true, Book.isFullCellId(cellID));
        cellID = "A1";
        assertEquals(false, Book.isFullCellId(cellID));
    }

    @Test
    public void addSheet() {
        newBook();
        book.addSheet("testsheet");
        assertEquals(true, book.sheetExists("testsheet"));
    }

    @Test
    public void sheetFromFullId() {
        newBook();
        String cellID = "!StuffTest.A1";
        assertEquals("StuffTest", Book.sheetFromFullId(cellID));
    }
    
    @Test
    public void getCells() {
        newBook();
        book.setOnActiveSheet("A1", "= 1 + 1");
        book.setOnActiveSheet("A2", "= 1 + 3");
        assertEquals(2,book.getCells().size());
    }

    @Test
    public void listFromRangeOrdered() {
        ArrayList<String> cells = Book.listFromRange("A1:B2");
        assertEquals(4, cells.size());
        assert (cells.contains("A1") && cells.contains("A2") && cells.contains("B1") && cells.contains("B2"));
    }

    @Test
    public void listFromRangeUnordered() {
        ArrayList<String> cells = Book.listFromRange("B2:A1");
        assertEquals(4, cells.size());
        assert (cells.contains("A1") && cells.contains("A2") && cells.contains("B1") && cells.contains("B2"));
    }

}
