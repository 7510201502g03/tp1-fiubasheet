package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class JsonTest {

    @Test
    public void saveAsJson() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        book.addSheet("sheet1");
        book.set("A1", "sheet1", "Test TDD");

        Json json = new Json();
        String testFile = "proy1.json";
        json.save(book, testFile);

        try {
            FileReader fr = new FileReader(testFile);
            String expectedResult = "{\n" + "  \"name\": \"" + testFile + "\",\n"
                    + "  \"version\": \"1.1\",\n" + "  \"cells\": [\n"
                    + "    {\n" + "      \"sheet\": \"sheet1\",\n"
                    + "      \"cellID\": \"A1\",\n"
                    + "      \"value\": \"Test TDD\",\n"
                    + "      \"type\": \"\",\n"
                    + "      \"format\": \"\"\n"
                    + "    "
                    + "}\n" + "  ]\n"
                    + "}\n";

            String jsonResult = "";
            int valor = fr.read();
            while (valor != -1) {
                jsonResult += (char) valor;
                valor = fr.read();
            }
            fr.close();
            assertEquals(expectedResult, jsonResult);

        } catch (IOException e) {
            fail("I/O Error");
        }

        // Cleanup
        File ftestFile = new File(testFile);
        ftestFile.delete();
    }


    @Test
    public void loadJson() {
        Json json = new Json();
        Book book = json.load("files/myJsonLoadTest.json");

        assertEquals(2, book.getCells().size());

        java.util.Iterator<Cell> it = book.getCells().iterator();

        Cell cell = it.next();

        assertEquals("sheet1", cell.getSheet());
        assertEquals("A1", cell.getcellID());
        assertEquals("=1 + 2", cell.getExpression());

        cell = it.next();

        assertEquals("sheet1", cell.getSheet());
        assertEquals("A3", cell.getcellID());
        assertEquals("=3 + 2", cell.getExpression());

    }
}