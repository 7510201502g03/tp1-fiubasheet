package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

public class CsvTest {

    @Test
    public void saveAsCsv() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        book.addSheet("sheet1");
        book.setActiveSheet("sheet1");
        book.setOnActiveSheet("A1", "=1 + 2");
        book.setOnActiveSheet("A3", "=2 + 2");
        book.set("A3", "default", "=2 + 2");
        book.setActiveSheet("default");

        Csv csv = new Csv();
        
        String testFile = "files/myCsv.csv";
        csv.save(book, testFile);
        
        // Cleanup
        File ftestFile = new File(testFile);
        ftestFile.delete(); 
    }

    @Test
    public void loadCsv() {
        Csv csv = new Csv();
        Book book = csv.load("files/myCsvLoadTest.csv");

        assertEquals("5", book.getFromActiveSheet("A6").getAsStringFormatted());
    }

}
