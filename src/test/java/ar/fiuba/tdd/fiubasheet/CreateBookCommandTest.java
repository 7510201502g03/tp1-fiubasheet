package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import static org.junit.Assert.*;

public class CreateBookCommandTest {

    @Test
    public void executeCreateBook() {
        FiubaSheet fiubaSheet = new FiubaSheet();
        ICommand command = new CreateBookCommand(fiubaSheet, "aBook");
        assertEquals(false, fiubaSheet.containsBook("aBook"));

        command.execute();
        assertEquals(true, fiubaSheet.containsBook("aBook"));
    }

    @Test
    public void unExecuteCreateBook() {
        FiubaSheet fiubaSheet = new FiubaSheet();
        ICommand command = new CreateBookCommand(fiubaSheet, "aBook");
        assertEquals(false, fiubaSheet.containsBook("aBook"));

        command.execute();
        assertEquals(true, fiubaSheet.containsBook("aBook"));

        command.unexecute();
        assertEquals(false, fiubaSheet.containsBook("aBook"));


    }

}
