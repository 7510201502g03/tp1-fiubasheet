package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class CellTest {

    private static final double DELTA = 0.00001;

    private class MockCalculator implements IExpressionCalculator {
        private HashMap<String, String> results = new HashMap<>();
        private HashMap<String, ArrayList<String>> depends = new HashMap<>();

        public MockCalculator() {
            results.put("2 + 3", "5.0");
            results.put("5 - 1", "4.0");
            results.put("A1", "5.0");
            ArrayList<String> dep = new ArrayList<>();
            dep.add("A1");
            depends.put("A1", dep);
            ArrayList<String> dep2 = new ArrayList<>();
            depends.put("2 + 3", dep2);
            depends.put("5 - 1", dep2);
        }

        @Override
        public String eval(String exp, String sheet) {
            return results.get(exp);
        }

        @Override
        public ArrayList<String> getDependencies(String expression, String sheet) {
            return depends.get(expression);
        }
    }

    private Cell cell;
    private CellEventNotifier cen = new CellEventNotifier();
    private IExpressionCalculator calc = new MockCalculator();

    @Test
    public void testCreateCell() throws Exception {
        cell = new Cell("A1", "Sheet", "=2 + 3", cen, calc);
        cell.calculateValue();
        assertEquals(5.0, cell.getAsDouble(), DELTA);
    }

    @Test
    public void testSetValue() throws Exception {
        cell = new Cell("A1", "Sheet", "=2 + 3", cen, calc);
        cell.setValue("=5 - 1");
        assertEquals(4.0, cell.getAsDouble(), DELTA);
    }

    @Test
    public void testSetCellDependantValue() throws Exception {
        cell = new Cell("A1", "Sheet", "=2 + 3", cen, calc);
        cell.calculateValue();
        cell = new Cell("D1", "Sheet", "=A1", cen, calc);
        cell.calculateValue();
        assertEquals(5.0, cell.getAsDouble(), DELTA);
    }

    @Test
    public void testGetExpression() {
        cell = new Cell("A1", "Sheet", "=2 + 3", cen, calc);
        assertEquals("=2 + 3", cell.getExpression());
    }
    
    @Test
    public void testGetCellID() {
        cell = new Cell("A1", "Sheet", "=2 + 3", cen, calc);
        assertEquals("A1", cell.getcellID());
    }
    
    @Test
    public void testGetSheet() {
        cell = new Cell("A1", "Sheet", "=2 + 3", cen, calc);
        assertEquals("Sheet", cell.getSheet());
    }
}
