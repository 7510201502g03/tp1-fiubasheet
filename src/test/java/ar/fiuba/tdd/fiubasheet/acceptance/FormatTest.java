package ar.fiuba.tdd.fiubasheet.acceptance;

import ar.fiuba.tdd.fiubasheet.FiubaSheet;
import ar.fiuba.tdd.fiubasheet.driver.BadFormatException;
import ar.fiuba.tdd.fiubasheet.driver.SpreadSheetTestDriver;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class FormatTest {

    private SpreadSheetTestDriver testDriver;

    @Before
    public void setUp() {
        testDriver = new FiubaSheet();
    }

    @Test
    public void stringIsNotNumber() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "String");
        testDriver.getCellValueAsDouble("tecnicas", "default", "A1");
    }

    // Pide el valor pero quiere la expresion
    @Test
    public void formulaAsString() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "= 1 + 2");
        testDriver.setCellType("tecnicas", "default", "A1", "String");
        assertEquals("= 1 + 2", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateFormatEuropean() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "dd-MM-YYYY");
        assertEquals("14-07-2012", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateFormatAmerican() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "MM-dd-YYYY");
        assertEquals("07-14-2012", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void dateFormatISO8601() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "YYYY-MM-dd");
        assertEquals("2012-07-14", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
    
    @Test
    public void dateNoAutoformat() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2012-07-14T00:00:00Z");
        assertEquals("2012-07-14T00:00:00Z", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
    
    @Test(expected = RuntimeException.class)
    public void badDateFormat() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "Not a date");
        testDriver.setCellType("tecnicas", "default", "A1", "Date");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "format", "YYYY-MM-DD");
        assertEquals("Error:BAD_DATE", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void moneyFormatUSD() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S ");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "decimal", "0");
        assertEquals("U$S 2", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }

    @Test
    public void moneyFormatUSDAndDecimal() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "2");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "decimal", "2");
        assertEquals("U$S2.00", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
    
    @Test(expected = NumberFormatException.class)
    public void badMoneyFormat() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "Not money");
        testDriver.setCellType("tecnicas", "default", "A1", "Currency");
        testDriver.setCellFormatter("tecnicas", "default", "A1", "symbol", "U$S");
        assertEquals("Error:BAD_CURRENCY", testDriver.getCellValueAsString("tecnicas", "default", "A1"));
    }
    
    @Test
    public void formulaAsNumber() {
        testDriver.createNewWorkBookNamed("tecnicas");
        testDriver.setCellValue("tecnicas", "default", "A1", "1");
        testDriver.setCellValue("tecnicas", "default", "A2", "=A1 + 1");
  
        assertEquals("2", testDriver.getCellValueAsString("tecnicas", "default", "A2"));
    }
}
