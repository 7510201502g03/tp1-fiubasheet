package ar.fiuba.tdd.fiubasheet;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class FormulaInspectorTest {

    FormulaInspector formulaInspector = new FormulaInspector();

    @Before
    public void setUp() {


    }

    @Test
    public void testSimpleFormula() throws Exception {
        //A1 = A2 + A3
        ArrayList<String> deps = new ArrayList<>();
        deps.add("A2");
        deps.add("A3");
        assertEquals(false, formulaInspector.hasCycles("A1", deps));
    }

    @Test
    public void testCyclicFormula() throws Exception {
        //A1 = A2 + A3
        ArrayList<String> deps = new ArrayList<>();
        deps.add("A2");
        deps.add("A3");
        assertEquals(false, formulaInspector.hasCycles("A1", deps));
        //A2 = A1
        deps.clear();
        deps.add("A1");
        assertEquals(true, formulaInspector.hasCycles("A2", deps));
    }
}