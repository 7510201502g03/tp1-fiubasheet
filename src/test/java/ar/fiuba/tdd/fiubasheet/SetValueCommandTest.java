package ar.fiuba.tdd.fiubasheet;

import org.junit.Test;

import static org.junit.Assert.*;

public class SetValueCommandTest {

    private static final double DELTA = 0.00001;

    @Test
    public void setValue() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        String expression = "= 2 + 1";
        ICommand command = new SetValueCommand(book, "default", "A1", expression);
        command.execute();
        String resultado = book.getFromActiveSheet("A1").getAsString();

        assertEquals(3.0, Float.parseFloat(resultado), DELTA);

    }

    @Test
    public void changeValueCell() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        book.setOnActiveSheet("A1", "= 1 + 1");
        String expression = "=2 + 1";
        ICommand command = new SetValueCommand(book,"default", "A1", expression);
        command.execute();
        String resultado = book.getFromActiveSheet("A1").getAsString();

        assertEquals(3.0, Float.parseFloat(resultado), DELTA);

    }

    @Test
    public void undoSetValue() {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        book.setOnActiveSheet("A1", "= 1 + 1");
        ICommand command = new SetValueCommand(book, "default", "A1", "=2 + 1");
        command.execute();
        String resultado = book.getFromActiveSheet("A1").getAsString();
        assertEquals(3.0, Float.parseFloat(resultado), DELTA);

        command.unexecute();
        resultado = book.getFromActiveSheet("A1").getAsString();
        assertEquals(2.0, Float.parseFloat(resultado), DELTA);
    }

}
