package ar.fiuba.tdd.fiubasheet;

import java.util.Locale;

public class CellFormatNumber implements ICellFormat {

    private int numberDecimals = 0;
    
    public CellFormatNumber(){}

    // El formato string es passthrough, no aplica ningun formato y matchea siempre por default
    public boolean matchesExpression(String expr) {
        return expr.matches("^[0-9]+(\\.[0-9]+)?$");
    }

    public String valueFromFormattedExpression(String expr) {
        assert (matchesExpression(expr));
        return Double.toString(Double.parseDouble(expr));
    }

    public String formatValue(String value) {
        Double val = Double.parseDouble(value);
        return String.format(Locale.ENGLISH, "%." + Integer.toString(this.numberDecimals) + "f", val);
    }

    // Configuracion de formato

    public void customize(String formatter, String format) {
        this.numberDecimals = Integer.parseInt(format);
        if (this.numberDecimals < 0) {
            throw new IllegalArgumentException("CellFormatNumber: invalid param");
        }
    }

    public String getType() {
        return "Number";
    }

    public String getFormat() {
        return String.valueOf(numberDecimals);
    }

    public void onSet(Cell cell) {

    }

}

