package ar.fiuba.tdd.fiubasheet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;

/** Permite manejar las entradas y salidas de archivos json. */
public class Json implements ImporterExporter {

    String name;
    final String version;

    ArrayList<Cell> cells = new ArrayList<Cell>();

    public Json() {
        version = "1.1";
    }

    public void save(Book book, String fileName) {
        name = fileName;
        
        for (Cell cell : book.getCells()) {
            cells.add(cell);
        }

        GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting();
        gsonBuilder.registerTypeAdapter(Cell.class, new SerializerCell());
        String jsonString = gsonBuilder.disableHtmlEscaping().create()
                .toJson(this);

        try {
            File file = new File(name);
            Writer writer = new OutputStreamWriter(new FileOutputStream(file),
                    "UTF-8");
            System.setProperty("line.separator", "\n");
            PrintWriter pw = new PrintWriter(writer);
            
            pw.println(jsonString);
            pw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public Book load(String fileName) {
        Book book;
        try {
            Reader reader = new InputStreamReader(
                    new FileInputStream(fileName), "UTF-8");
            Gson gson = new GsonBuilder().registerTypeAdapter(Book.class,
                    new DeserializerBook()).create();
            book = gson.fromJson(reader, Book.class);

            return book;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}