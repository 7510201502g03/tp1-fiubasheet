package ar.fiuba.tdd.fiubasheet;

import java.util.ArrayList;
import java.util.function.BiFunction;

public abstract class CalcFuncComp implements ICalcFunction {

    public String evaluateComp(String range, ICellValueReader cellreader, String sheet, BiFunction<Double, Double, Boolean> comp) {
        ArrayList<String> cells = Book.listFromRange(range);
        Double max = cellreader.getValue(cells.get(0), sheet);
        for (String cell : cells) {
            Double cellVal = cellreader.getValue(cell, sheet);
            max = comp.apply(cellVal, max) ? cellVal : max;
        }
        return Double.toString(max);
    }

    public ArrayList<String> getDependencies(String range, String sheet) {
        return Calculator.makeFullCellIdDependencies(Book.listFromRange(range), sheet);
    }

}

