package ar.fiuba.tdd.fiubasheet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

public class InputHandler {

    private FiubaSheet fiubaSheet;
    private InputCommandFactory inputCommandFactory = new InputCommandFactory();

    public InputHandler(FiubaSheet fiubaSheet) {
        this.fiubaSheet = fiubaSheet;
    }

    public void readEvalPrint() {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
        String input;
        boolean exit = false;
        try {
            while (!exit && (input = in.readLine()) != null ) {
                exit = eval(input);
            }
        } catch (IOException ex) {
            System.out.print("Error: Couldn't read command\n");
        }
    }

    public boolean eval(String input) {
        String[] splitInput = input.split("\\s+");
        String commandName = splitInput[0];
        if (commandName.equals("quit")) {
            return true;
        }
        try {
            InputCommand inputCommand = inputCommandFactory.createInputCommand(commandName);
            inputCommand.eval(splitInput, fiubaSheet);
        } catch (IllegalArgumentException ex) {
            System.out.print("Error: invalid command\n");
        } /*catch (Exception ex) {
            System.out.print("Error: could not execute command\n");
        }*/
        return false;

    }

}
