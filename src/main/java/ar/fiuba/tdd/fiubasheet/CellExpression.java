package ar.fiuba.tdd.fiubasheet;

class CellExpression implements IOperation {

    private String cellId;

    public CellExpression(String cellId) {
        this.cellId = cellId;
    }

    public Double evaluate(ICellValueReader cellreader, String sheet) {
        return cellreader.getValue(cellId, sheet);
    }

}
