package ar.fiuba.tdd.fiubasheet;

class OperationExpression implements IOperation {

    private IBinOp operation;
    public IOperation left;
    public IOperation right;

    public OperationExpression(IBinOp operation) {
        this.operation = operation;
    }

    public Double evaluate(ICellValueReader cellreader, String sheet) {
        Double lhs = left.evaluate(cellreader, sheet);
        Double rhs = right.evaluate(cellreader, sheet);
        return operation.exec(lhs, rhs);
    }

}
