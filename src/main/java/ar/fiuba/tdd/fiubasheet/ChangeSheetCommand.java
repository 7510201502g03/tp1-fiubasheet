package ar.fiuba.tdd.fiubasheet;

//Comando que permite cambiar la hoja donde se tiene foco y deshacer el cambio
public class ChangeSheetCommand implements ICommand {
    private Book book;
    private String sheet;
    private String oldSheet;

    @Override
    public void execute() {
        this.book.setActiveSheet(sheet);
    }

    @Override
    public void unexecute() {
        this.book.setActiveSheet(oldSheet);

    }

    public ChangeSheetCommand(Book book, String sheet) {
        this.oldSheet = book.getActiveSheet();
        this.sheet = sheet;
        this.book = book;
    }

}
