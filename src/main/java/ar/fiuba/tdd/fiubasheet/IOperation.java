package ar.fiuba.tdd.fiubasheet;

public interface IOperation {

    // Takes a cell accessor and a context
    Double evaluate(ICellValueReader cellreader, String sheet);

}

