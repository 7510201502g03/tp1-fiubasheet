package ar.fiuba.tdd.fiubasheet;

import java.util.Stack;

//Guarda un historial de comandos ejecutados y permite hacer undo/redo sobre los mismos
public class CommandHandler {

    private Stack<ICommand> undoCommands = new Stack<>();
    private Stack<ICommand> redoCommands = new Stack<>();


    public void undo() {
        if (!undoCommands.isEmpty()) {
            ICommand command = undoCommands.pop();
            command.unexecute();
            redoCommands.push(command);
        }
    }

    public void redo() {
        if (!redoCommands.isEmpty()) {
            ICommand command = redoCommands.pop();
            command.execute();
            redoCommands.push(command);
        }
    }

    public void doCommand(ICommand command) {
        command.execute();
        insertCommand(command);
        redoCommands.clear();

    }

    private void insertCommand(ICommand command) {
        undoCommands.push(command);
    }



}
