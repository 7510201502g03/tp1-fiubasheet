package ar.fiuba.tdd.fiubasheet;

import ar.fiuba.tdd.fiubasheet.driver.BadFormulaException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Evalua una expresion matematica, que puede contener celdas como operandos
public class Calculator implements IExpressionCalculator {

    private HashMap<String, IBinOp> binOps = new HashMap<>();
    private HashMap<String, ICalcFunction> calcFunctions = new HashMap<>();
    private ICellValueReader reader;

    public Calculator() {
        // Load bin ops
        binOps.put("+", new BinOpSum());
        binOps.put("-", new BinOpSub());
        // Load functions
        calcFunctions.put("AVERAGE", new CalcFuncAvg());
        calcFunctions.put("MIN", new CalcFuncMin());
        calcFunctions.put("MAX", new CalcFuncMax());
        calcFunctions.put("CONCAT", new CalcFuncConcat(this));

    }

    public void setBookReader(ICellValueReader reader) {
        this.reader = reader;
    }

    // Evalua una expresion y devuelve el resultado como string
    // El argumento sheet se utilizar para resolver las referencias a celdas que no tienen un sheet explicito
    public String eval(String expression, String sheet) {
        // Switch between formula and function here
        Matcher matcher = Pattern.compile("^\\s*([A-Z]+)\\((.*)\\)$").matcher(expression);
        if (matcher.find() && calcFunctions.containsKey(matcher.group(1))) {
            return calcFunctions.get(matcher.group(1)).evaluate(matcher.group(2), reader, sheet);
        } else {
            return Double.toString(buildTree(toPostfix(expression.trim())).evaluate(reader, sheet));
        }
    }

    //Devuelve todas las celdas que aparezcan referenciadas en la formulagitgi
    public ArrayList<String> getDependencies(String expression, String sheet) {
        ArrayList<String> dependencies;
        Matcher matcher = Pattern.compile("^\\s*([A-Z]+)\\((.*)\\)$").matcher(expression);
        if (matcher.find() && calcFunctions.containsKey(matcher.group(1))) {
            dependencies = getDependenciesFunction(matcher.group(1), matcher.group(2), sheet);
        } else {
            dependencies = getDependenciesFormula(expression);
        }

        return makeFullCellIdDependencies(dependencies, sheet);

    }

    public static ArrayList<String> makeFullCellIdDependencies(ArrayList<String> deps, String sheet) {
        ArrayList<String> newDeps = new ArrayList<>();
        for ( String cell : deps ) {
            if (!Book.isFullCellId(cell)) {
                newDeps.add(Book.makeFullCellId(cell, sheet));
            } else {
                newDeps.add(cell);
            }
        }
        return newDeps;
    }

    public ArrayList<String> getDependenciesFunction(String func, String args, String sheet) {
        return calcFunctions.get(func).getDependencies(args, sheet);
    }

    public ArrayList<String> getDependenciesFormula(String expression) {
        ArrayList<String> dependencies = new ArrayList<>();
        String[] tokens = expression.trim().split(" ");

        for (String token : tokens) {
            // If token is a number or variable
            if (isOperator(token)) {
                continue;
            }
            try {
                Double res = Double.parseDouble(token);
            } catch (NumberFormatException ex) {
                if (Book.isFullCellId(token) || Book.isValidCellId(token)) {
                    dependencies.add(token);
                } else {
                    throw new BadFormulaException();
                }
            }
        }
        return dependencies;
    }

    private boolean isOperator(String op) {
        return (this.binOps.get(op) != null);
    }

    // Decide si un token va para el stack o se agrega al buffer
    private void tokenMultiplex(String token, Stack<String> opStack, StringBuffer out) {
        if (!isOperator(token)) {
            out.append(token + " ");
        } else {
            if (!opStack.isEmpty()) {
                out.append(opStack.pop() + " ");
            }
            opStack.push(token);
        }
    }

    public String toPostfix(String expr) {

        Stack<String> opStack = new Stack<String>();
        StringBuffer out = new StringBuffer();

        String[] tokens = expr.split(" ");

        for (String token : tokens) {
            tokenMultiplex(token, opStack, out);
        }

        if (!opStack.isEmpty()) {
            out.append(opStack.pop());
        }

        return out.toString().trim();
    }

    public IOperation buildTree(String expression) {

        Stack<IOperation> stack = new Stack<IOperation>();
        String[] tokens = expression.split(" ");

        for (String token : tokens) {
            // If token is a number or variable
            if (!isOperator(token)) {
                IOperation term = null;
                try {
                    term = new NumberExpression(Double.parseDouble(token));
                } catch (NumberFormatException ex) {
                    if (Book.isFullCellId(token) || Book.isValidCellId(token)) {
                        term = new CellExpression(token);
                    } else {
                        throw new BadFormulaException();
                    }
                }
                stack.push(term);
            } else {
                OperationExpression expr = new OperationExpression(binOps.get(token));
                expr.right = stack.pop();
                expr.left = stack.pop();
                stack.push(expr);
            }
        }

        return stack.pop();
    }

}

