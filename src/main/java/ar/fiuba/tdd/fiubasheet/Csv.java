package ar.fiuba.tdd.fiubasheet;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;

/** Permite manejar las entradas y salidas de archivos csv. */

public class Csv implements ImporterExporter {

    public void save(Book book, String fileName) {
        try {
            File file = new File(fileName);

            FileOutputStream output = new FileOutputStream(file);
            OutputStreamWriter streamWriter = new OutputStreamWriter(output,
                    "utf-8");
            BufferedWriter writer = new BufferedWriter(streamWriter);
            CsvWriter csvWriter = new CsvWriter(writer, ',');

            csvWriter.write("Sheet");
            csvWriter.write("CellID");
            csvWriter.write("Value");
            csvWriter.endRecord();

            for (Cell cell : book.getCells()) {
                String activeSheet = book.getActiveSheet();
                if (activeSheet.equals(cell.getSheet())) {
                    csvWriter.write(cell.getSheet());
                    csvWriter.write(cell.getcellID());
                    csvWriter.write(cell.getAsStringFormatted());
                    csvWriter.endRecord();
                }
            }

            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Book load(String fileName) {
        Book book = new Book(new Calculator(), new CellEventNotifier());

        try {
            Reader reader = new InputStreamReader(
                    new FileInputStream(fileName), "UTF-8");
            CsvReader csvReader = new CsvReader(reader);
            csvReader.readHeaders();

            while (csvReader.readRecord()) {
                if (!book.sheetExists(csvReader.get("Sheet"))) {
                    book.addSheet(csvReader.get("Sheet"));
                    book.setActiveSheet(csvReader.get("Sheet"));
                }
                book.setOnActiveSheet(csvReader.get("CellID"),
                        csvReader.get("Value"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return book;

    }

}
