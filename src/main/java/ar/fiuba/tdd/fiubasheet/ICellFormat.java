package ar.fiuba.tdd.fiubasheet;

public interface ICellFormat {

    // True if the input matches the format
    public boolean matchesExpression(String expr);

    // Extracts value from a formatted expression
    public String valueFromFormattedExpression(String expr);

    // Formats a raw value with the current format
    public String formatValue(String value);

    // Configuracion de formato
    public void customize(String key, String value);

    public String getType();

    public String getFormat();

    // Cuando se setea el formato de una celda, hay que actualizar el valor interno
    public void onSet(Cell cell);

}

