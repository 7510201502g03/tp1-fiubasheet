package ar.fiuba.tdd.fiubasheet;

import java.util.HashMap;
import java.util.Locale;

public class CellFormatMoney implements ICellFormat {

    private HashMap<String, String> attributes = new HashMap<>();

    public CellFormatMoney() {
        attributes.put("symbol", "$");
        attributes.put("decimal", "2");
    }

    // El formato string es passthrough, no aplica ningun formato y matchea siempre por default
    public boolean matchesExpression(String expr) {
        return expr.matches("^\\$[0-9]+(\\.[0-9]+)?$");
    }

    public String valueFromFormattedExpression(String expr) {
        assert (matchesExpression(expr));
        return Double.toString(Double.parseDouble(expr.substring(1)));
    }

    public String formatValue(String value) {
        Double val = Double.parseDouble(value);
        return String.format(Locale.ENGLISH, attributes.get("symbol") + "%." + attributes.get("decimal") + "f", val);
    }

    // Configuracion de formato
    // Param 1 = decimals
    // Param 2 = currency symbol
    public void customize(String formatter, String format) {
        attributes.remove(formatter);
        attributes.put(formatter, format);

    }

    public String getType() {
        return "Money";
    }

    public String getFormat() {
        return attributes.get("symbol") + attributes.get("decimal");
    }

    public void onSet(Cell cell) {

    }
}

