package ar.fiuba.tdd.fiubasheet;

public class BinOpSum implements IBinOp {

    public Double exec(Double lhs, Double rhs) {
        return lhs + rhs;
    }

}

