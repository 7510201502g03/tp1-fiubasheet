package ar.fiuba.tdd.fiubasheet;

public interface Subscriber {

    void update();
}
