package ar.fiuba.tdd.fiubasheet;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class CellFormatDate implements ICellFormat {

    String format = "dd-MM-yy";

    // El formato string es passthrough, no aplica ningun formato y matchea siempre por default
    public boolean matchesExpression(String expr) {
        return expr.matches("^[0-9]{1,2}-[0-9]{1,2}-[0-9]{2,4}$");
    }

    public String valueFromFormattedExpression(String expr) {

        //DateFormat formatter = new DateTime("yyyy-MM-dd'T'HH:mm:ssZ");
        Date date;
        //try {
            //date = formatter.parse(expr);
        date = javax.xml.bind.DatatypeConverter.parseDateTime(expr).getTime();
        //} catch (IllegalArgumentException ex) {
        //    throw new IllegalArgumentException("CellFormatDate: invalid value");
        //}
        return Long.toString(date.getTime());
    }

    public String formatValue(String value) {
        DateFormat formatter = new SimpleDateFormat(this.format);
        formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = new Date();
        date.setTime(Long.parseLong(value));
        return formatter.format(date);
    }

    public void customize(String formatter, String format) {
        this.format = format;
    }

    public String getType() {
        return "Date";
    }

    public String getFormat() {
        return format;
    }

    public void onSet(Cell cell) {
        cell.setValueFromFormattedExpression();
    }

}

