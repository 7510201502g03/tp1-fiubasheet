package ar.fiuba.tdd.fiubasheet;

import java.util.HashMap;

public class FormatFactory {

    HashMap<String, ICellFormat> formats = new HashMap<>();

    public FormatFactory() {
        // Cargamos los formatos posibles
        // El orden es importante
        formats.put("DATE", new CellFormatDate());
        formats.put("CURRENCY", new CellFormatMoney());
        formats.put("NUMBER", new CellFormatNumber());
        formats.put("STRING", new CellFormatString());
    }

    public ICellFormat guessFormat(String expr) throws Exception {
        for (ICellFormat format : formats.values()) {
            if (format.matchesExpression(expr)) {
                return format.getClass().newInstance();
            }
        }
        // Fallback
        return new CellFormatString();
    }

    public ICellFormat makeFormat(String name) throws Exception {
        if (!formats.containsKey(name.toUpperCase())) {
            return null; //throw new IllegalArgumentException("Invalid format");
        }
        return formats.get(name.toUpperCase()).getClass().newInstance();
    }

}
