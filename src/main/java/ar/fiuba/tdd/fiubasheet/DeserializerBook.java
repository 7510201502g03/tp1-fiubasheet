package ar.fiuba.tdd.fiubasheet;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class DeserializerBook implements JsonDeserializer<Book> {

    @Override
    public Book deserialize(JsonElement json, Type typeOfT,
            JsonDeserializationContext context) throws JsonParseException {
        Book book = new Book(new Calculator(), new CellEventNotifier());

        final JsonObject jsonObject = json.getAsJsonObject();
        JsonArray cellsArray = jsonObject.get("cells").getAsJsonArray();

        final String[] cells = new String[cellsArray.size()];
        for (int i = 0; i < cells.length; i++) {
            final JsonElement cell = cellsArray.get(i);
            JsonObject cellObject = cell.getAsJsonObject();
            String sheet = cellObject.get("sheet").getAsString();
            String cellId = cellObject.get("cellID").getAsString();
            String expression = cellObject.get("value").getAsString();
            String type = cellObject.get("type").getAsString();
            String format = cellObject.get("format").getAsString();
            if (!book.sheetExists(sheet)) {
                book.addSheet(sheet);
            }
            book.set(cellId, sheet, expression);
            try {
                book.setFormat(cellId, sheet, type);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return book;

    }

}
