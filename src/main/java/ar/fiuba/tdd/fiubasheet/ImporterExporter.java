package ar.fiuba.tdd.fiubasheet;

/** Interfaz de la cual se implementarán los Importadores/Exportadores. */

public interface ImporterExporter {
    
    void save(Book book, String fileName);
    
    Book load(String fileName);
    
}
