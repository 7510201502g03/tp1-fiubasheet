package ar.fiuba.tdd.fiubasheet;

import java.util.HashMap;
import java.util.List;

public class InputCommandFactory {

    private HashMap<String, InputCommand> commands;

    public InputCommandFactory() {
        commands = new HashMap<>();
        createBook();
        getBook();
        setgetCell();
        deleteBook();
        undo();
        redo();
        load();
        save();
        setType();
        setFormat();
    }

    public InputCommand createInputCommand(String commandName) {
        if (commands.containsKey(commandName)) {
            return commands.get(commandName);
        } else {
            throw new IllegalArgumentException();
        }
    }

    private void assertRequiredInputNumber(String[] input, int number) {
        if (input.length != number) {
            throw new IllegalArgumentException();
        }
    }

    private void createBook() {
        commands.put("createBook", (String[] input, FiubaSheet fiubaSheet) -> {
                assertRequiredInputNumber(input, 2);
                fiubaSheet.createNewWorkBookNamed(input[1]);
            });
        commands.put("createSheet", (String[] input, FiubaSheet fiubaSheet) -> {
                assertRequiredInputNumber(input, 3);
                fiubaSheet.createNewWorkSheetNamed(input[1], input[2]);
            });
    }


    private void getBook() {
        commands.put("getBook", (String[] input, FiubaSheet fiubaSheet) -> {
                List<String> list = fiubaSheet.getWorkBookValues(input[1]);
                for (String element : list) {
                    System.out.print(element + "\n");
                }
            });
    }

    private void setgetCell() {
        commands.put("setCellValue", (String[] input, FiubaSheet fiubaSheet) -> {
                if (input.length != 5) {
                    throw new IllegalArgumentException();
                }
                fiubaSheet.setCellValue(input[1], input[2], input[3], input[4]);
            });
        commands.put("getCellValue", (String[] input, FiubaSheet fiubaSheet) -> {
                assertRequiredInputNumber(input, 4);
                fiubaSheet.getCellValueAsString(input[1], input[2], input[3]);
            });
    }

    private void deleteBook() {
        commands.put("deleteBook", (String[] input, FiubaSheet fiubaSheet) -> {
                assertRequiredInputNumber(input, 2);
                fiubaSheet.removeBook(input[1]);
            });
    }

    private void undo() {
        commands.put("undo", (String[] input, FiubaSheet fiubaSheet) -> {
                assertRequiredInputNumber(input, 1);
                fiubaSheet.undo();
            });
    }

    private void redo() {
        commands.put("redo", (String[] input, FiubaSheet fiubaSheet) -> {
                assertRequiredInputNumber(input, 1);
                fiubaSheet.redo();
            });
    }

    private void load() {
        commands.put("save", (String[] input, FiubaSheet fiubaSheet) -> {
                setUpSerializer(input, fiubaSheet);
                fiubaSheet.persistWorkBook(input[1], input[2]);
            });
    }

    private void save() {
        commands.put("load", (String[] input, FiubaSheet fiubaSheet) -> {
                setUpSerializer(input, fiubaSheet);
                fiubaSheet.reloadPersistedWorkBook(input[1], input[2]);
            });
    }

    private void setUpSerializer(String[] input, FiubaSheet fiubaSheet) {
        assertRequiredInputNumber(input, 3);
        ImporterExporter serializer = ImporterExporterFactory.getImpExp(input[2]);
        fiubaSheet.setSerializer(serializer);
    }

    private void setType() {
        commands.put("setType", (String[] input, FiubaSheet fiubaSheet) -> {
                assertRequiredInputNumber(input, 5);
                fiubaSheet.setCellType(input[1], input[2], input[3], input[4]);
            });
    }

    private void setFormat() {
        commands.put("setFormat", (String[] input, FiubaSheet fiubaSheet) -> {
                assertRequiredInputNumber(input, 6);
                fiubaSheet.setCellFormatter(input[1], input[2], input[3], input[4], input[5]);
            });
    }
}
