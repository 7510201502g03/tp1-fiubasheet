package ar.fiuba.tdd.fiubasheet;

import java.util.ArrayList;

public interface IExpressionCalculator {

    String eval(String expression, String sheet);

    ArrayList<String> getDependencies(String expression, String sheet);

}
