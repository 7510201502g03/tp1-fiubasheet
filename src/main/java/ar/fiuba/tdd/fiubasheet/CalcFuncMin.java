package ar.fiuba.tdd.fiubasheet;

public class CalcFuncMin extends CalcFuncComp {

    public String evaluate(String range, ICellValueReader cellreader, String sheet) {
        return super.evaluateComp(range, cellreader, sheet, (Double current, Double actual) -> { return current < actual; });
    }

}

