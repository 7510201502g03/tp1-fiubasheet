package ar.fiuba.tdd.fiubasheet;

/** Clase que se encarga de crear el tipo de ImporterExporter. */

public class ImporterExporterFactory {

    public static ImporterExporter getImpExp(String fileName) {

        String[] fields = fileName.split("\\.");
        String extension = fields[1];

        if (extension.equals("json")) {
            return new Json();
        } else if (extension.equals("csv")) {
            return new Csv();
        }

        return null;

    }
}
