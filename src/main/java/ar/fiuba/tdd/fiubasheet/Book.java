package ar.fiuba.tdd.fiubasheet;

import ar.fiuba.tdd.fiubasheet.driver.BadReferenceException;
import ar.fiuba.tdd.fiubasheet.driver.UndeclaredWorkSheetException;

import java.util.*;

//Contenedor de celdas. Administra el manejo y creacion de hojas y celdas dentro del book.
public class Book {

    private Calculator calculator;
    private CellEventNotifier cellEventNotifier;
    private String activeSheet;
    private HashMap<String, Cell> cells = new HashMap<>();
    private TreeSet<String> sheets = new TreeSet<>();
    private FormulaInspector formulaInspector = new FormulaInspector();
    private FormatFactory formatFactory = new FormatFactory();

    public static class RangeEdges {
        public int startCol;
        public int startRow;
        public int endCol;
        public int endRow;
    }

    public Book(Calculator calculator, CellEventNotifier cellEventNotifier) {
        this.calculator = calculator;
        this.calculator.setBookReader(new CellValueReader(this));
        this.cellEventNotifier = cellEventNotifier;
        addSheet("default");
        setActiveSheet("default");
    }

    // Set value for cell in arbitrary sheet
    public void set(String cellId, String sheet, String expression) {
        assertValidCellId(cellId);
        assertValidSheetId(sheet);
        assertValidSheet(sheet);
        String fullId = makeFullCellId(cellId, sheet);
        Cell cell = this.get(fullId);
        if (Cell.isFormula(expression) && expressionHasCycles(cell, fullId, Cell.getFormula(expression))) {
            throw new BadReferenceException();
        }
        cell.setValue(expression);
    }

    // Set value for cell in active sheet
    public void setOnActiveSheet(String cellId, String expression) {
        set(cellId, this.activeSheet, expression);
    }

    // Get cell from arbitrary sheet
    public Cell get(String cellId, String sheet) {
        assertValidCellId(cellId);
        assertValidSheetId(sheet);
        assertValidSheet(sheet);
        return this.get(makeFullCellId(cellId, sheet));
    }

    // Get cell from fully qualified Id (with sheet)
    // Create if it doesn't exist
    public Cell get(String fullId) {
        String cellId = cellFromFullId(fullId);
        String sheet = sheetFromFullId(fullId);
        assertValidCellId(cellId);
        assertValidSheetId(sheet);
        if (cells.get(fullId) == null) {
            Cell newCell = new Cell(cellId, sheet, null, this.cellEventNotifier, this.calculator);
            cells.put(fullId, newCell);
        }
        return cells.get(fullId);
    }

    // Get cell from active sheet
    public Cell getFromActiveSheet(String cellId) {
        assertValidCellId(cellId);
        // Convert the cell to active sheet if no sheet is detected
        String fullId = makeFullCellId(cellId, this.activeSheet);
        if (cells.get(fullId) == null) {
            cells.put(fullId, new Cell(cellId, sheetFromFullId(fullId), "", this.cellEventNotifier, this.calculator));
        }
        return cells.get(fullId);
    }

    // Generate string id from cellId+sheet
    public static String makeFullCellId(String cellId, String sheet) {
        return "!" + sheet + "." + cellId;
    }

    // True si el id de la celda incluye el Sheet
    public static boolean isFullCellId(String cellId) {
        return cellId.charAt(0) == '!' && cellId.split("\\.").length == 2;
    }

    // Gets the sheet part from a full cell id
    // Eg: "Stuff:A1" => "Stuff"
    public static String sheetFromFullId(String id) {
        if (isFullCellId(id)) {
            String[] tokens = id.substring(1).split("\\.");
            return tokens[0];
        } else {
            throw new IllegalArgumentException("Invalid full cell id");
        }
    }

    // Gets the sheet part from a full cell id
    // Eg: "Stuff:A1" => "Stuff"
    public static String cellFromFullId(String id) {
        if (isFullCellId(id)) {
            String[] tokens = id.split("\\.");
            return tokens[1];
        } else {
            throw new IllegalArgumentException("Invalid full cell sheet");
        }
    }

    // Column to numbers
    public static int columnToInt(char column) {
        // ASCII('A') = 65
        return ((int)column) - 65;
    }

    public static char intToColumn(int val) {
        // ASCII('A') = 65
        return (char)(val + 65);
    }

    public static boolean assertValidRange(String range) {
        String[] tokens = range.split(":");
        String exMessage = "Invalid range: " + range;
        if (tokens.length != 2 || !Book.isValidCellId(tokens[0]) || !Book.isValidCellId(tokens[1])) {
            throw new IllegalArgumentException(exMessage);
        }
        return true;
    }

    private static RangeEdges rangeToIndices(String range) {
        Book.assertValidRange(range);
        RangeEdges ret = new Book.RangeEdges();
        String[] tokens = range.split(":");
        try {
            ret.startCol = Book.columnToInt(tokens[0].charAt(0));
            ret.startRow = Integer.parseInt(tokens[0].substring(1));
            ret.endCol = Book.columnToInt(tokens[1].charAt(0));
            ret.endRow = Integer.parseInt(tokens[1].substring(1));
            // Swap if not ordered
            if (ret.startCol > ret.endCol) {
                ret.startCol = ret.endCol;
                ret.endCol = Book.columnToInt(tokens[0].charAt(0));
            }
            if (ret.startRow > ret.endRow) {
                ret.startRow = ret.endRow;
                ret.endRow = Integer.parseInt(tokens[0].substring(1));
            }
        } catch (NumberFormatException ex) {
            throw new IllegalArgumentException("Invalid range: " + range);
        }
        return ret;
    }

    public static ArrayList<String> listFromRange(String range) {
        ArrayList<String> ret = new ArrayList<>();
        RangeEdges edges = rangeToIndices(range);
        // Generate list from new range
        for (int r = edges.startRow;r <= edges.endRow;r++) {
            for (int c = edges.startCol;c <= edges.endCol;c++) {
                String cellId = Book.intToColumn(c) + Integer.toString(r);
                ret.add(cellId);
            }
        }
        return ret;
    }

    public void setActiveSheet(String sheet) {
        assertValidSheet(sheet);
        this.activeSheet = sheet;
    }

    public String getActiveSheet() {
        return this.activeSheet;
    }

    public void addSheet(String sheet) {
        assertValidSheetId(sheet);
        if (sheetExists(sheet)) {
            throw new IllegalArgumentException("Sheet" + sheet + " already exists");
        }
        sheets.add(sheet);
    }

    public void removeSheet(String sheet) {
        assertValidSheet(sheet);
        if (getActiveSheet().equals(sheet)) {
            throw new IllegalArgumentException("Cannot remove active sheet");
        }
        // Sheet should be empty before removing (!)
        sheets.remove(sheet);
    }

    public List<String> getSheetNames() {
        return  new LinkedList<>(sheets);
    }

    public boolean sheetExists(String sheet) {
        return sheets.contains(sheet);
    }

    // Throw exception if sheet invalid
    public void assertValidSheet(String sheet) {
        if (!sheetExists(sheet)) {
            throw new UndeclaredWorkSheetException();
        }
    }

    public static boolean isValidCellId(String id) {
        return id.matches("^[A-Z][0-9]+$");
    }

    public static boolean isValidSheetId(String sheet) {
        return sheet.matches("^[A-Za-z\\s0-9]+$");
    }

    // Throw exception if cell id is invalid
    public static void assertValidCellId(String id) {
        if (!isValidCellId(id)) {
            throw new IllegalArgumentException("Invalid cell id");
        }
    }

    // Throw exception if sheet id is invalid
    public static void assertValidSheetId(String sheet) {
        if (!isValidSheetId(sheet)) {
            throw new IllegalArgumentException("Invalid sheet id");
        }
    }
    
    public Collection<Cell> getCells() {
        return cells.values();
    }

    public List<String> getCellsAsString() {
        LinkedList<String> list = new LinkedList<>();
        String cellContent;
        for (Map.Entry<String, Cell> entry : cells.entrySet()) {
            cellContent = entry.getKey() + ": " + entry.getValue().getAsStringFormatted();
            list.add(cellContent);
        }
        return list;
    }

    private boolean expressionHasCycles(Cell cell, String fullId, String expression) {

        String oldFormula = cell.getExpression();
        ArrayList<String> oldDeps = null;
        if (oldFormula != null && Cell.isFormula(oldFormula)) {
            oldDeps = calculator.getDependencies(Cell.getFormula(oldFormula), cell.getSheet());
            formulaInspector.removeEdges(fullId, oldDeps);
        }

        ArrayList<String> deps = calculator.getDependencies(expression, cell.getSheet());
        boolean hasCycle = formulaInspector.hasCycles(fullId, deps);

        if (oldDeps != null && hasCycle) {
            formulaInspector.addEdges(fullId, oldDeps);
        }

        return hasCycle;
    }

    // Set value for cell in arbitrary sheet
    public void setFormat(String cellId, String sheet, String format) throws Exception {
        assertValidCellId(cellId);
        assertValidSheetId(sheet);
        assertValidSheet(sheet);
        String fullId = makeFullCellId(cellId, sheet);
        Cell cell = this.get(fullId);
        cell.setFormat(formatFactory.makeFormat(format));
    }

    // Set value for cell in active sheet
    public void setFormatOnActiveSheet(String cellId, String format) throws Exception {
        setFormat(cellId, this.activeSheet, format);
    }

    public void customizeFormat(String cellId, String sheet, String formatter, String format) throws Exception {
        assertValidCellId(cellId);
        assertValidSheetId(sheet);
        assertValidSheet(sheet);
        String fullId = makeFullCellId(cellId, sheet);
        Cell cell = this.get(fullId);
        cell.customizeFormat(formatter, format);
    }

    public int getCountSheets() {
        return (sheets.size() - 1);
    }


}
