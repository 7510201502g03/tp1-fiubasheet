package ar.fiuba.tdd.fiubasheet;


import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

import java.util.ArrayList;

public class FormulaInspector {

    DefaultDirectedGraph<String, DefaultEdge> graph =
            new DefaultDirectedGraph<>(DefaultEdge.class);


    boolean hasCycles(String cell, ArrayList<String> dependencies) {
        graph.addVertex(cell);
        for (String dep : dependencies) {
            graph.addVertex(dep);
            graph.addEdge(cell, dep);
        }

        CycleDetector<String, DefaultEdge> cycleDetector = new CycleDetector<>(graph);
        boolean detected = cycleDetector.detectCycles();

        if (detected) {
            removeEdges(cell, dependencies);
        }
        return detected;
    }


    public void removeEdges(String cell, ArrayList<String> dependencies) {
        for (String dep : dependencies) {
            graph.removeEdge(cell, dep);
        }
    }

    public void addEdges(String cell, ArrayList<String> dependencies) {
        graph.addVertex(cell);
        for (String dep : dependencies) {
            graph.addVertex(dep);
            graph.addEdge(cell, dep);
        }
    }
}


