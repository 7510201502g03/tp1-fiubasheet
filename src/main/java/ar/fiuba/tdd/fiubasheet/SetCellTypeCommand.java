package ar.fiuba.tdd.fiubasheet;

public class SetCellTypeCommand implements ICommand {

    private Book workbook;
    private String cellID;
    private String format;
    private String oldFormat;
    private String sheet;

    @Override
    public void execute() {
        try {
            workbook.setFormat(cellID, sheet, format);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    @Override
    public void unexecute() {
        try {
            workbook.setFormat(cellID, sheet, oldFormat);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }

    public  SetCellTypeCommand(Book book, String sheet,  String cellID, String format) {
        setter(book, sheet, cellID, format);
        this.oldFormat = book.get(cellID, sheet).getCellType();

    }


    private void setter(Book workbook, String worksh,  String cellID, String format) {
        this.workbook = workbook;
        this.sheet = worksh;
        this.cellID = cellID;
        this.format = format;
    }
}