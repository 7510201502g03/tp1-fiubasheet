package ar.fiuba.tdd.fiubasheet;

import java.util.ArrayList;

public class CalcFuncAvg implements ICalcFunction {

    public String evaluate(String range, ICellValueReader cellreader, String sheet) {
        ArrayList<String> cells = Book.listFromRange(range);
        Double total = 0.0;
        for (String cell : cells) {
            total += cellreader.getValue(cell, sheet);
        }
        return Double.toString(total / cells.size());
    }

    public ArrayList<String> getDependencies(String range, String sheet) {
        return Calculator.makeFullCellIdDependencies(Book.listFromRange(range), sheet);
    }

}

