package ar.fiuba.tdd.fiubasheet;

import java.util.ArrayList;

public class CalcFuncConcat implements ICalcFunction {

  //  private IExpressionCalculator calculator;

    public CalcFuncConcat(IExpressionCalculator calculator) {
   //     this.calculator = calculator;
    }

    public String evaluate(String args, ICellValueReader cellreader, String sheet) {
        String result = "";
        ArrayList<String> params = getParamsFromArgs(args);
        for (String param : params) {
            result = result.concat(cellreader.getValueLiteral(param, sheet));
        }
        return result;
    }

    public ArrayList<String> getDependencies(String args, String sheet) {
        return getParamsFromArgs(args);
        /*
        ArrayList<String> deps = new ArrayList<>();
        ArrayList<String> params = getParamsFromArgs(args);
        for (String param : params) {
            if (Cell.isFormula(param)) {
                deps.addAll(this.calculator.getDependencies(param, sheet));
            }
        }
        return deps;*/
    }

    // Convierte el string the argumentos en parametros individuales
    // "=a+1 , b, c` -> Array("=a+1", "b", "c")
    private static ArrayList<String> getParamsFromArgs(String args) {
        ArrayList<String> params = new ArrayList<>();
        String[] tokens = args.split(",");
        for (String token : tokens) {
            params.add(token.trim());
        }
        return params;
    }

}

