package ar.fiuba.tdd.fiubasheet;

public interface IBinOp {

    Double exec(Double lhs, Double rhs);

}

