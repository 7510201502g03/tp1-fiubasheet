package ar.fiuba.tdd.fiubasheet;

public interface ICellValueReader {

    Double getValue(String cellId, String sheet);

    String getValueLiteral(String cellId, String sheet);

}

