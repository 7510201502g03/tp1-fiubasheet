package ar.fiuba.tdd.fiubasheet;

import java.util.ArrayList;

public interface ICalcFunction {

    String evaluate(String args, ICellValueReader cellreader, String sheet);

    ArrayList<String> getDependencies(String args, String sheet);

}

