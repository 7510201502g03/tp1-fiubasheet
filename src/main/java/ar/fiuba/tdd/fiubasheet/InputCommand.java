package ar.fiuba.tdd.fiubasheet;

public interface InputCommand {
    void eval(String[] input, FiubaSheet fiubaSheet);
}
