package ar.fiuba.tdd.fiubasheet;

//Comando que permite asignar valor a una celda y deshacerlo
public class SetValueCommand implements ICommand {

    private Book book;
    private String cellID;
    private String expression;
    private String oldExpression;
    private String sheet;

    @Override
    public void execute() {
        this.book.set(this.cellID, this.sheet, this.expression);
    }

    @Override
    public void unexecute() {
        this.book.set(this.cellID, this.sheet, this.oldExpression);

    }

    public SetValueCommand(Book book, String sheet,  String cellID, String expression) {
        this.book = book;
        this.cellID = cellID;
        this.expression = expression;
        this.sheet = sheet;
        this.oldExpression = book.get(cellID, sheet).getExpression();

    }

}
