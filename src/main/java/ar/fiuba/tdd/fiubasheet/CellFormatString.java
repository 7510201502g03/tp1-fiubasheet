package ar.fiuba.tdd.fiubasheet;

public class CellFormatString implements ICellFormat {

    public CellFormatString(){}
    // El formato string es passthrough, no aplica ningun formato y matchea siempre por default
    // No tiene nada configurable
    public boolean matchesExpression(String expr) {
        return true;
    }

    public String valueFromFormattedExpression(String expr) {
        return expr;
    }

    public String formatValue(String value) {
        return value;
    }

    public void customize(String formatter, String format) {  }

    public String getType() {
        return "String";
    }

    public String getFormat() {
        return "";
    }

    public void onSet(Cell cell) {
        cell.setValueAsExpression();
    }

}

