package ar.fiuba.tdd.fiubasheet;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

/** Se utiliza para seleccionar los campos a serializar. */
public class SerializerCell implements JsonSerializer<Cell> {

    @Override
    public JsonElement serialize(Cell cell, Type typeOfSrc,
            JsonSerializationContext context) {

        JsonObject jobject = new JsonObject();
        jobject.addProperty("sheet", cell.getSheet());
        jobject.addProperty("cellID", cell.getcellID());
        jobject.addProperty("value", cell.getExpression());
        jobject.addProperty("type", cell.getCellType());
        jobject.addProperty("format", cell.getCellFormat());

        return jobject;
    }

}