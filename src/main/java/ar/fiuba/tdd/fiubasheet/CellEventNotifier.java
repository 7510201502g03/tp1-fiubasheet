package ar.fiuba.tdd.fiubasheet;

import java.util.ArrayList;
import java.util.HashMap;

//Notifica a todos los subscribers cuando ocurre un evento en el topic en el que se inscribieron (Pub/sub)
public class CellEventNotifier {

    private HashMap<String, ArrayList<Subscriber>> topics = new HashMap<>();

    public void subscribe(String topic, Subscriber sub) {
        ArrayList<Subscriber> subs = getSubscribers(topic);
        if (subs != null) {
            subs.add(sub);
        } else {
            ArrayList<Subscriber> newsub = new ArrayList<>();
            newsub.add(sub);
            topics.put(topic, newsub);
        }
    }

    public void unsubscribe(Subscriber sub) {
        for (ArrayList<Subscriber> subs : topics.values()) {
            subs.remove(sub);
        }
    }

    public void notifySubscribers(String topic) {
        ArrayList<Subscriber> subs = getSubscribers(topic);
        if (subs != null) {
            for (Subscriber sub : subs) {
                sub.update();
            }
        }
    }

    public ArrayList<Subscriber> getSubscribers(String topic) {
        return topics.get(topic);
    }
}
