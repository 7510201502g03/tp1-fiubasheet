package ar.fiuba.tdd.fiubasheet;

class NumberExpression implements IOperation {

    private Double value;

    public NumberExpression(Double value) {
        this.value = value;
    }

    public Double evaluate(ICellValueReader cellreader, String sheet) {
        return value;
    }

}
