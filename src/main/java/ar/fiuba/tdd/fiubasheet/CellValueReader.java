package ar.fiuba.tdd.fiubasheet;

//Proxy de book que solo permite leer los valores de una celda
public class CellValueReader implements ICellValueReader {

    private Book book;

    public CellValueReader(Book book) {
        this.book = book;
    }

    // Get value de una celda dado el contexto sheet
    public Double getValue(String cellId, String sheet) {
        if (Book.isFullCellId(cellId)) {
            return this.book.get(cellId).getAsDouble();
        } else {
            return this.book.get(cellId, sheet).getAsDouble();
        }
    }

    public String getValueLiteral(String cellId, String sheet) {
        if (Book.isFullCellId(cellId)) {
            return this.book.get(cellId).getAsStringFormatted();
        } else {
            return this.book.get(cellId, sheet).getAsStringFormatted();
        }
    }

}
