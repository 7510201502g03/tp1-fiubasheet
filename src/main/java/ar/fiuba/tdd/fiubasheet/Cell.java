package ar.fiuba.tdd.fiubasheet;

import ar.fiuba.tdd.fiubasheet.driver.BadFormulaException;

import java.util.ArrayList;

//Almacena el contenido de una celda de una planilla. Delega el calculo de su formula matematica a IExpressionCalculator
public class Cell implements Subscriber {

    private String cellID;
    private String expression;
    private String sheet;
    private String value;
    private IExpressionCalculator calculator;
    private CellEventNotifier cellEvents;
    private ICellFormat format;

    public Cell(String cellID, String sheet, String expression,
            CellEventNotifier cellEvents, IExpressionCalculator calculator) {
        this.cellID = cellID;
        this.sheet = sheet;
        this.cellEvents = cellEvents;
        this.calculator = calculator;
        this.expression = expression;
        this.value = null;
        // Formato default devuelve la expresion
        this.format = null;
        //setValue(expression);
    }

    public String getCellID() {
        return cellID;
    }

    public void setFormat(ICellFormat format) {
        this.format = format;
        if (format != null) {
            this.format.onSet(this);
        }
    }

    public void setValueFromFormattedExpression() {
        this.value = format.valueFromFormattedExpression(this.expression);
    }

    public void setValueAsExpression() {
        this.value = this.expression;
    }

    public void customizeFormat(String formatter, String format) {
        this.format.customize(formatter, format);
    }

    public String getAsString() {
        return (this.value != null ? this.value : (this.expression != null ? this.expression : ""));
    }

    public String getAsStringFormatted() {
        if (format == null) {
            return this.expression;
        } else {
            return format.formatValue(this.getAsString());
        }
    }

    public Double getAsDouble() {
        if (this.value == null || this.value.isEmpty()) {
            return 0.0;
        } else {
            try {
                return Double.parseDouble(this.getAsString());
            } catch (NumberFormatException ex) {
                throw new BadFormulaException();
            }
        }
    }

    public void setValue(String expression) {
        cellEvents.unsubscribe(this);
        this.expression = expression;
        if (isFormula(this.expression)) {
            try {
                ArrayList<String> cellsDep = calculator
                        .getDependencies(getFormula(expression), this.sheet);
                for (String cellInFormula : cellsDep) {
                    cellEvents.subscribe(cellInFormula, this);
                }
                calculateValue();
                //this.setFormat(new CellFormatNumber());
            } catch (BadFormulaException ex) {
                // BadFormula default value = null
                this.value = null;
                throw ex;
            }
        } else {
            this.value = this.expression;
            cellEvents.notifySubscribers(Book.makeFullCellId(this.cellID, this.sheet));
        }
    }

    public void update() {
        calculateValue();
    }

    public void calculateValue() {
        if (isFormula(this.expression)) {
            // Caso especial CONCAT funciona con strings aunque es una formula
            if (this.expression.matches("=\\s*CONCAT.*")) {
                this.format = new CellFormatString();
            }
            if (this.format == null) {
                this.format = new CellFormatNumber();
            }
            this.value = calculator.eval(getFormula(expression), this.sheet);
            cellEvents.notifySubscribers(Book.makeFullCellId(this.cellID, this.sheet));
        }
    }

    // Funciones static para detectar y extraer formulas
    public static boolean isFormula(String expression) {
        return (expression.length() > 0 && expression.charAt(0) == '=');
    }

    public static String getFormula(String expression) {
        return expression.substring(1);
    }

    public String getExpression() {
        return this.expression == null ? "" : this.expression;
    }
    
    public String getcellID() {
        return this.cellID;        
    }
    
    public String getSheet() {
        return this.sheet;        
    }

    public String getCellType() {
        return format == null ? "" : format.getType();
    }

    public String getCellFormat() {
        return format == null ? "" : format.getFormat();
    }
}
