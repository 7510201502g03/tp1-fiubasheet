package ar.fiuba.tdd.fiubasheet;

import ar.fiuba.tdd.fiubasheet.driver.SpreadSheetTestDriver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//Contiene los books existentes y delega la ejecucion de los comandos requeridos
public class FiubaSheet implements SpreadSheetTestDriver {

    // private Book activeBook;
    private CommandHandler commandHandler;
    private HashMap<String, Book> books = new HashMap<>();
    private ImporterExporter serializer;

    public FiubaSheet() {
        commandHandler = new CommandHandler();
        serializer = new Json();
    }

    public boolean isEmpty() {
        return books.isEmpty();
    }

    public void setSerializer(ImporterExporter ser) {
        serializer = ser;
    }

    public boolean containsBook(String bookName) {
        return books.containsKey(bookName);
    }

    public void addBook(String bookName) {
        Book book = new Book(new Calculator(), new CellEventNotifier());
        books.put(bookName, book);
    }

    public void removeBook(String bookName) {
        books.remove(bookName);
    }

    //implementacion test drive
    public void undo() {
        commandHandler.undo();
    }

    public void redo() {
        commandHandler.redo();
    }

    @Override
    public void setCellType(String workBookName, String workSheetName, String cellId, String type) {
        Book book = books.get(workBookName);
        ICommand command = new SetCellTypeCommand(book, workSheetName, cellId, type);
        commandHandler.doCommand(command);
    }

    @Override
    public void setCellFormatter(String workBookName, String workSheetName, String cellId, String formatter, String format) {
        Book book = books.get(workBookName);
        try {
            book.customizeFormat(cellId, workSheetName, formatter, format);
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }


    public List<String> workBooksNames() {
        List<String> list = new ArrayList<>();
        list.addAll(books.keySet());
        return list;
    }

    public void createNewWorkBookNamed(String name) {
        ICommand command = new CreateBookCommand(this, name);
        commandHandler.doCommand(command);
    }

    public void createNewWorkSheetNamed(String workbookName, String name) {
        Book book = books.get(workbookName);
        ICommand command = new CreateSheetCommand(book, name);
        commandHandler.doCommand(command);
    }

    public List<String> workSheetNamesFor(String workBookName) {
        Book book = books.get(workBookName);
        return book.getSheetNames();
    }

    public void setCellValue(String workBookName, String workSheetName, String cellId, String value) {
        Book book = books.get(workBookName);
        ICommand command = new SetValueCommand(book, workSheetName, cellId, value);
        commandHandler.doCommand(command);
    }

    public String getCellValueAsString(String workBookName, String workSheetName, String cellId) {
        Book book = books.get(workBookName);
        return book.get(cellId, workSheetName).getAsStringFormatted();
    }

    public double getCellValueAsDouble(String workBookName, String workSheetName, String cellId) {
        Book book = books.get(workBookName);
        return book.get(cellId, workSheetName).getAsDouble();
    }

    public List<String> getWorkBookValues(String bookName) {
        Book book = books.get(bookName);
        return book.getCellsAsString();
    }

    public void persistWorkBook(String bookName, String filename) {
        Book bookToSave = books.get(bookName);
        serializer = new Json();
        serializer.save(bookToSave, filename);
    }

    @Override
    public void saveAsCSV(String workBookName, String sheetName, String fileName) {
        serializer = new Csv();
        Book booktoSave = books.get(workBookName);
        booktoSave.setActiveSheet(sheetName);
        serializer.save(booktoSave, fileName);

    }

    @Override
    public void loadFromCSV(String workBookName, String sheetName, String fileName) {
        serializer = new Csv();
        Book bookToLoad = serializer.load(fileName);
        books.put(workBookName, bookToLoad);
    }

    @Override
    public int sheetCountFor(String workBookName) {
        Book book = books.get(workBookName);
        return book.getCountSheets();
    }

    @Override
    public void reloadPersistedWorkBook(String workBookName, String fileName) {
        serializer = new Json();
        Book bookToLoad = serializer.load(fileName);
        books.put(workBookName, bookToLoad);
    }

}

