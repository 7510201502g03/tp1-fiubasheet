package ar.fiuba.tdd.fiubasheet;

//Comando que permite crear una planilla y deshacerla
public class CreateBookCommand implements ICommand {

    private FiubaSheet fiubaSheet;
    private String bookName;

    @Override
    public void execute() {
        this.fiubaSheet.addBook(this.bookName);
    }

    @Override
    public void unexecute() {
        this.fiubaSheet.removeBook(this.bookName);
    }

    public CreateBookCommand(FiubaSheet fiubaSheet, String bookName) {
        this.bookName = bookName;
        this.fiubaSheet = fiubaSheet;
    }

}
