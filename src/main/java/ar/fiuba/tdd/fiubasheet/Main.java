package ar.fiuba.tdd.fiubasheet;


public class Main {

    public static void main(String[] args) {
        FiubaSheet fiubaSheet = new FiubaSheet();
        InputHandler inputHandler = new InputHandler(fiubaSheet);
        inputHandler.readEvalPrint();
    }
}
