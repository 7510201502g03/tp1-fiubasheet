package ar.fiuba.tdd.fiubasheet;

//Permite crear una nueva hoja dentro de una planilla, y deshacer su creacion
public class CreateSheetCommand implements ICommand {

    private Book book;
    private String sheet;

    @Override
    public void execute() {
        book.addSheet(this.sheet);
    }

    @Override
    public void unexecute() {
        book.removeSheet(this.sheet);
    }

    public CreateSheetCommand(Book book, String sheet) {
        this.book = book;
        this.sheet = sheet;

    }

}
