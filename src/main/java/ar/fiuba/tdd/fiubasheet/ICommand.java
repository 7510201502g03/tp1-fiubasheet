package ar.fiuba.tdd.fiubasheet;

/**
 * Created by nico on 24/09/15.
 */
public interface ICommand {

    void execute();

    void unexecute();

}
